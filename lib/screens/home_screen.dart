import 'package:flutter/material.dart';
import 'package:rqcardapp/screens/card_loading_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'statistics_screen.dart';
import 'settings_screen.dart';
import 'test_screen.dart';
import 'login_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  SharedPreferences _userSharedPreferences;

  void _logout() async {
    await _userSharedPreferences.remove("username");
    await _userSharedPreferences.remove("password");
    await _userSharedPreferences.remove("token");

    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginScreen()),);
  }

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) => _userSharedPreferences = value);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('RQ'),
        leading: IconButton(
          icon: Icon(Icons.timeline),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => StatisticsScreen()),);
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsScreen()),);
            },
          ),
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: _logout
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                  width: 200,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    color: Colors.blue,
                    elevation: 10,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          leading: Icon(Icons.done_all, size: 30),
                          title: Text('TEST', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                          subtitle: Text('Yeni bir test başlat.', style: TextStyle(color: Colors.white)),
                        ),
                        ButtonTheme(
                          child: ButtonBar(
                            alignment: MainAxisAlignment.center,
                            children: <Widget>[
                                FlatButton(
                                  child: const Text('Start', style: TextStyle(color: Colors.blue)),
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => TestScreen()),);
                                  },
                                  color: Colors.white,
                                  splashColor: Colors.lightBlueAccent,
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: 200,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    color: Colors.white,
                    elevation: 10,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          leading: Icon(Icons.input, size: 30, color: Colors.black,),
                          title: Text('CARD', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                          subtitle: Text('Kartlar ile çalışmaya başla.', style: TextStyle(color: Colors.black)),
                        ),
                        ButtonTheme(
                          child: ButtonBar(
                            alignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FlatButton(
                                child: const Text('Start', style: TextStyle(color: Colors.white)),
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => CardLoadingScreen()),);
                                },
                                color: Colors.black,
                                splashColor: Colors.white70,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flip_card/flip_card.dart';
import 'package:rqcardapp/models/card_model.dart';
import 'package:audioplayers/audioplayers.dart';
import 'home_screen.dart';

class CardScreen extends StatefulWidget {
  CardScreen({this.cardList});
  final cardList;

  @override
  _CardScreenState createState() => _CardScreenState();
}

class _CardScreenState extends State<CardScreen> {
  List<CardModel> _cardList;
  CardModel _selectedCard;
  bool more = true;
  String status = "Sıradaki";
  AudioPlayer audioPlayer = AudioPlayer();

  play(String url) async {
    int result = await audioPlayer.play(url);
    if (result == 1) {
      debugPrint("ÇAl KEKE ÇAL");
    }
  }

  newSelectedCard() {
    if(status != "Bitir") {
      if(_cardList.length > 0) {
        setState(() {
          _selectedCard = _cardList[0];
        });

        _cardList.removeAt(0);

        debugPrint(_selectedCard.name);
      } else {
        setState(() {
          more = false;
          status = "Bitir";
        });
        debugPrint("List Finished");
      }
    } else {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()),);
    }
  }

  @override
  void initState() {
    super.initState();

    _cardList = widget.cardList;
    newSelectedCard();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('CARD'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          if (more) Center(
            child: Container(
              height: 300,
              width: 300,
              child: Card(
                elevation: 0.0,
                margin: EdgeInsets.only(left: 32.0, right: 32.0, top: 20.0, bottom: 0.0),
                color: Color(0x00000000),
                child: FlipCard(
                  direction: FlipDirection.HORIZONTAL,
                  speed: 1000,
                  onFlipDone: (status) {
                    print(status);
                  },
                  front: Container(
                    decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        if(_selectedCard.frontFaceImg != null)
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5.0),
                            child: Image.network(_selectedCard.frontFaceImg)
                          ),
                        Text("${_selectedCard.frontFace}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30)),
                        if(_selectedCard.frontFaceSound != null)
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: IconButton(
                              icon: Icon(Icons.volume_up),
                              tooltip: 'Listen Sound',
                              onPressed: () {
                                play(_selectedCard.frontFaceSound);
                              },
                            ),
                          )
                      ],
                    ),
                  ),
                  back: Container(
                    decoration: BoxDecoration(
                      color: Colors.white38,
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("${_selectedCard.backFace}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ) else Center(child: Text("🐣 Çalışmamız Bitti", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30))),
        ]
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          newSelectedCard();
        },
        icon: Icon(Icons.navigate_next, color: Colors.white,),
        label: Text('$status', style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.black,
      ),
    );
  }
}

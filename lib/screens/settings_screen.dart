import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsScreen extends StatefulWidget {
  @override
   _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final snackBar = SnackBar(
    content: Text('Değişiklikler Kaydedildi!', style: TextStyle(color: Colors.white)),
    duration: Duration(seconds: 2),
    backgroundColor: Colors.teal,
    action: SnackBarAction(
      textColor: Colors.white,
      onPressed: () {},
      label: 'OK',
    ),
  );

  String _testValue, _cardValue;
  SharedPreferences _userSharedPreferences;

  void _save() {
    _userSharedPreferences.setString("test", _testValue);
    _userSharedPreferences.setString("card", _cardValue);
  }

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) {
      _userSharedPreferences = value;

      setState(() {
        _testValue = _userSharedPreferences.get("test");
        _cardValue = _userSharedPreferences.get("card");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text('SETTINGS'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text('Test ile gösterilecek soru sayısı:', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20)),
                  DropdownButton<String>(
                      value: _testValue,
                      items: [
                        DropdownMenuItem(
                          value: "5",
                          child: Text("5"),
                        ),
                        DropdownMenuItem(
                          value: "10",
                          child: Text("10"),
                        ),DropdownMenuItem(
                          value: "15",
                          child: Text("15"),
                        ),DropdownMenuItem(
                          value: "20",
                          child: Text("20"),
                        ),
                      ],
                      onChanged: (String newTestValue) {
                        setState(() {
                          _testValue = newTestValue;
                        });
                      }
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text('Card ile gösterilecek soru sayısı:', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20)),
                DropdownButton<String>(
                    value: _cardValue,
                    items: [
                      DropdownMenuItem(
                        value: "5",
                        child: Text("5"),
                      ),
                      DropdownMenuItem(
                        value: "10",
                        child: Text("10"),
                      ),DropdownMenuItem(
                        value: "15",
                        child: Text("15"),
                      ),DropdownMenuItem(
                        value: "20",
                        child: Text("20"),
                      ),
                    ],
                    onChanged: (String newCardValue) {
                      setState(() {
                        _cardValue = newCardValue;
                      });
                    }
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                RaisedButton(
                  color: Colors.black,
                  onPressed: () {
                    _save();

                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  },
                  child: Text('Save', style: TextStyle(fontSize: 20),),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
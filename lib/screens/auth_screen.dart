import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

import 'home_screen.dart';
import 'login_screen.dart';


class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  var _username, _password;
  SharedPreferences _userSharedPreferences;

  void userAuth() async {
    if (_username == null || _password == null) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginScreen()),);
    } else {
      var url = 'http://10.0.2.2:8000/api/token/';
      var reqBody = {};
      reqBody["username"] = _username;
      reqBody["password"] = _password;

      var response = await http.post(
          url,
          headers: <String, String>{
            'Content-Type': 'application/json',
          },
          body: convert.jsonEncode(reqBody)
      );

      var jsonData = convert.jsonDecode(response.body);

      if (jsonData["access"] != null) {
        await _userSharedPreferences.setString("token", jsonData["access"]);

        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()),);
      } else {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginScreen()),);
      }
    }
  }

  void _getUserPreferences() async {
    var username = await Future.value(_userSharedPreferences.getString("username")?? null);
    var password = await Future.value(_userSharedPreferences.getString("password")?? null);

    setState(() {
      _username = username;
      _password = password;
    });

    userAuth();
  }

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) {
      _userSharedPreferences = value;

      _getUserPreferences();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}

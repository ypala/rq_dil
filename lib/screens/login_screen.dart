import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'home_screen.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final warningSnackBar = SnackBar(
    content: Text('Bir Şeyler Ters Gitti!', style: TextStyle(color: Colors.white)),
    duration: Duration(seconds: 2),
    backgroundColor: Colors.red,
    action: SnackBarAction(
      textColor: Colors.white,
      onPressed: () {},
      label: 'OK',
    ),
  );


  String _username, _password;
  bool _autoValidation = false;
  SharedPreferences _userSharedPreferences;

  Future<String> userAuth() async {
    var url = 'http://10.0.2.2:8000/api/token/';
    var reqBody = {};
    reqBody["username"] = _username;
    reqBody["password"] = _password;

    var response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
      body: convert.jsonEncode(reqBody)
    );

    var jsonData = convert.jsonDecode(response.body);
    if (jsonData["access"] != null) {
      await _userSharedPreferences.setString("username", _username);
      await _userSharedPreferences.setString("password", _password);
      await _userSharedPreferences.setString("token", jsonData["access"]);
      await _userSharedPreferences.setString("test", "5");
      await _userSharedPreferences.setString("card", "5");

      debugPrint(jsonData["access"]);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()),);
    } else {
      _scaffoldKey.currentState.showSnackBar(warningSnackBar);
    }
    return response.body;
  }

  void _saveButton() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      userAuth();
    } else {
      setState(() {
        _autoValidation = true;
      });
    }
  }

  String _validUsername(String username) {
    if (username == null || username == '') {
      return 'Kullanıcı adı boş bırakılamaz';
    } else if (username.length < 5) {
      return 'Kullanıcı adı en az 5 karakter olmalı.';
    } else if (username.contains(' ')) {
      return 'Kullanıcı adı boşluk içeremez.';
    }else return null;
  }

  String _validPassword(String password) {
    if (password == null || password == '') {
      return 'Parola boş olamaz';
    } else if (password.length < 6) {
      return 'Parola en az 6 karakterden oluşmalı.';
    } else return null;
  }

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) => _userSharedPreferences = value);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text('LOGIN'),
      ),
      body: Center(
        child: Form(
          key: formKey,
          autovalidate: _autoValidation,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Devam Etmek İçin Lütfen Giriş Yapınız'),
              Padding(
                padding: const EdgeInsets.fromLTRB(50.0, 0, 50.0, 30.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Username',
                    prefixIcon: Icon(Icons.person),
                  ),
                  validator: _validUsername,
                  onSaved: (username) => _username = username,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50.0, 0, 50.0, 30.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Password',
                    prefixIcon: Icon(Icons.vpn_key)
                  ),
                  obscureText: true,
                  validator: _validPassword,
                  onSaved: (password) => _password = password,
                ),
              ),
              RaisedButton(
                onPressed: (){
                  _saveButton();
                },
                child: Text('LOGIN'),
                color: Colors.black,
              )
            ],
          ),
        ),
      ),
    );
  }
}

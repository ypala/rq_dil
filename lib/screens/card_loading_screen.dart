import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:rqcardapp/models/card_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'card_screen.dart';


class CardLoadingScreen extends StatefulWidget {
  @override
  _CardLoadingScreen createState() => _CardLoadingScreen();
}

class _CardLoadingScreen extends State<CardLoadingScreen> {
  SharedPreferences _userSharedPreferences;
  final String _url = 'http://10.0.2.2:8000/api/card/list/';
  List<CardModel> _cardList;

  Future getCardList() async {
    String number = _userSharedPreferences.getString('card');
    String token = _userSharedPreferences.getString("token");
    var newUrl = _url + number;

    var response = await http.get(
      newUrl,
      headers: {
        'Authorization': 'Bearer $token'
      },
    );

    _cardList = cardModelFromJson(response.body);

    if(_cardList != null){
     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CardScreen(cardList: _cardList,)),);
    }
  }

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) {
      _userSharedPreferences = value;

      getCardList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}

import 'dart:convert';

List<CardModel> cardModelFromJson(String str) => List<CardModel>.from(json.decode(str).map((x) => CardModel.fromJson(x)));

String cardModelToJson(List<CardModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CardModel {
  String name;
  String slug;
  String frontFace;
  String backFace;
  String frontFaceImg;
  dynamic frontFaceSound;
  dynamic backFaceSound;

  CardModel({
    this.name,
    this.slug,
    this.frontFace,
    this.backFace,
    this.frontFaceImg,
    this.frontFaceSound,
    this.backFaceSound,
  });

  factory CardModel.fromJson(Map<String, dynamic> json) => CardModel(
    name: json["name"] == null ? null : json["name"],
    slug: json["slug"] == null ? null : json["slug"],
    frontFace: json["front_face"] == null ? null : json["front_face"],
    backFace: json["back_face"] == null ? null : json["back_face"],
    frontFaceImg: json["front_face_img"] == null ? null : json["front_face_img"],
    frontFaceSound: json["front_face_sound"],
    backFaceSound: json["back_face_sound"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "slug": slug == null ? null : slug,
    "front_face": frontFace == null ? null : frontFace,
    "back_face": backFace == null ? null : backFace,
    "front_face_img": frontFaceImg == null ? null : frontFaceImg,
    "front_face_sound": frontFaceSound,
    "back_face_sound": backFaceSound,
  };
}
